const express = require("express");

//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods forr manipulation of our database

const mongoose = require("mongoose")

const app = express();

const port = 3001;
//MongoDB Atlas Connection
//when we want to use local mongoDB/robo3t
//mongoose.connect("mongodb://localhost:27017/databaseName")

mongoose.connect("mongodb+srv://Willie:W!llie050391@cluster0.w4hh5.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
		{
			//use to prevent connection problems
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
)
let db = mongoose.connection;

//connection error message
db.on("error", console.error.bind(console, "connection error"))
//connection successful message
db.once("open", () => console.log("We're connected to the cloud database"))


app.use(express.json());

app.use(express.urlencoded({ extended:true}));


//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database
//acts as a blueprint to our data
//Use Schema() constructor of the Mongoose module to create a new Schema object 

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		//default values are the predefined values for a field if we don't put any value.
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);
	//parameter(model) is in singular form and first letter is capital
	//Task is the collection name
	//taskSchema - kung ano ang ipapasok

//Routes/endpoints

//Create a new task

//Business Logic
/*
	1. Add a functionality to check if there  are duplicate tasks
		-If the task already exists in the database, we return error
		-If the task doesn't exist in the database, we add it in the database
			1.The task data will be coming from the requests body.
			2. Create a new Task object with field/property
			3. Save the object to our database.
*/

app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) => { 
		//If a document was found and the documents' name matches the information sent via the client
		if (result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else{
			//if no document was found
			//create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				//if there are errors in saving
				if (saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New task created")
				}

			})
		}
	})
})


//Get all tasks
//Business Logic
/*
	1. Find/Retrieve all the documents  
	2.if an error is encountered, print the error
	3. if no errors are found, send a success status back to the client and return an array of documents

*/

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				Message : "from the database",
				dataFromMDB: result
			})

		}
	})
})

/*

S30 Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.



Business Logic in Creating a user "/signup"
1.Add a functionality to check if there are duplicate tasks
	if the user already exists: "return error" or "already registered"
	if the user does not exist, we add it on our database
		1. The user data will be coming from the req.body
		2. Create a new User object with a "username" and "password" fields/properties
		3. Then save, and add an error handling
2.
3.
*/

//ACTIVITY S30

//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database
//acts as a blueprint to our data
//Use Schema() constructor of the Mongoose module to create a new Schema object 



//START OF S30 ACTIVITY

const signSchema = new mongoose.Schema({
	name: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
})

const Sign = mongoose.model("Sign", signSchema);


app.post("/signup", (req, res) => {
	Sign.findOne({ name: req.body.name, password:req.body.password}, (err, result) => { 
		
		if (result !== null && result.name === req.body.name,
			result !== null && result.password === req.body.password)
			{
			return res.send("Return Error: Already registered");
		} else{
			let newSign = new Sign({
				name: req.body.name,
				password: req.body.password,
			
			})

			newSign.save((saveErr, savedTask) => {
				//if there are errors in saving
				if (saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New signup created")
				}

			})
		}
	})
})


app.get("/users", (req, res) => {
	Sign.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				Message : "All recorded sign-ups",
				dataFromMDB: result
			})

		}
	})
})


//END of S30 activity



app.listen(port, () => console.log(`Server running at port ${port}`));

